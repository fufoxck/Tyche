﻿using System;
using System.Reflection.Emit;

namespace Tyche.Common.ProxyBuilder
{
    public interface ITycheProxyBuilder
    {
        void Build(ModuleBuilder moduleBuilder, Type type);
    }
}
