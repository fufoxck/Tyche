﻿using Newtonsoft.Json;

namespace Tyche.Common.Util.Models.Entity
{
    public class WeiXinJSSDKTicketEntity
    {
        /// <summary>
        /// 错误码
        /// </summary>
        [JsonProperty("errcode")]
        public int ErrorCode { get; set; }

        /// <summary>
        /// 错误信息
        /// </summary>
        [JsonProperty("errmsg")]
        public string ErrorMessage { get; set; }

        /// <summary>
        /// ticket
        /// </summary>
        [JsonProperty("ticket")]
        public string Ticket { get; set; }

        /// <summary>
        /// Expire
        /// </summary>
        [JsonProperty("expires_in")]
        public int Expire { get; set; }
    }
}
