﻿using System;

namespace Tyche.WebApi.HttpMethods
{
    [AttributeUsage(AttributeTargets.Method)]
    public class HttpPostAttribute : Attribute
    {
    }
}
