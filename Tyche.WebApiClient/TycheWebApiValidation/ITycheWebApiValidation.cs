﻿namespace Tyche.WebApiClient.TycheWebApiValidation
{
    public interface ITycheWebApiValidation
    {
        /// <summary>
        /// 验证是否合法
        /// </summary>
        /// <param name="value"></param>
        void TycheValid(object value);
    }
}
