﻿using System;
using System.Reflection;

namespace Tyche.Common.Util.Models.Entity
{
    class DynamicMethodEntity
    {
        /// <summary>
        /// 有效期
        /// </summary>
        public DateTime Expires { get; set; }

        /// <summary>
        /// 方法
        /// </summary>
        public MethodInfo Method { get; set; }
    }
}
