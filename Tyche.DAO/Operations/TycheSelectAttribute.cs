﻿using System;

namespace Tyche.DAO.Operations
{
    [AttributeUsage(AttributeTargets.Method)]
    public class TycheSelectAttribute : Attribute
    {
        /// <summary>
        /// 是否多条
        /// </summary>
        public bool Multiple { get; set; }

        /// <summary>
        /// 构造函数
        /// </summary>
        /// <param name="multiple"></param>
        public TycheSelectAttribute(bool multiple = false)
        {
            Multiple = multiple;
        }
    }
}
