﻿using System;

namespace Tyche.DAO.Operations
{
    [AttributeUsage(AttributeTargets.Method)]
    public class TycheUpdateAttribute : Attribute
    {
    }
}
