﻿using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Controllers;
using System.Web.Http.Dispatcher;
using Tyche.Common.Resource;

namespace Tyche.WebApiService.HttpControllerSelector
{
    public class TycheWebApiControllerSelector : DefaultHttpControllerSelector
    {
        private HttpConfiguration config { get; set; }

        public TycheWebApiControllerSelector(HttpConfiguration config) : base(config)
        {
            this.config = config;
        }

        public override HttpControllerDescriptor SelectController(HttpRequestMessage request)
        {
            var name = GetControllerName(request);
            return new HttpControllerDescriptor(config, name, TycheResourceManager.GetResource(name).Proxy);
        }
    }
}
