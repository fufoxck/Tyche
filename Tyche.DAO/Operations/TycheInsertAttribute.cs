﻿using System;

namespace Tyche.DAO.Operations
{
    [AttributeUsage(AttributeTargets.Method)]
    public class TycheInsertAttribute : Attribute
    {
        /// <summary>
        /// 是否返回Id
        /// </summary>
        public bool ReturnId { get; set; }

        /// <summary>
        /// 构造函数
        /// </summary>
        /// <param name="returnId"></param>
        public TycheInsertAttribute(bool returnId = false)
        {
            ReturnId = returnId;
        }
    }
}
