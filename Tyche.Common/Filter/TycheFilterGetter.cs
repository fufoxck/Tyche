﻿using System.Linq;
using System.Reflection;

namespace Tyche.Common.Filter
{
    public abstract class TycheFilterGetter
    {
        /// <summary>
        /// 获取所有Filter
        /// </summary>
        /// <param name="methodInfo"></param>
        /// <returns></returns>
        public static ITycheFilter[] GetFilters(MethodInfo methodInfo)
        {
            return methodInfo
                .GetCustomAttributes()
                .Where(a => a.GetType().BaseType == typeof(TycheFilterAttribute))
                .Select(x => { return (ITycheFilter)x; })
                .ToArray();
        }
    }
}
