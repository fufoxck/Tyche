﻿using System;

namespace Tyche.WebApi.HttpMethods
{
    [AttributeUsage(AttributeTargets.Method)]
    public class HttpGetAttribute : Attribute
    {
    }
}
