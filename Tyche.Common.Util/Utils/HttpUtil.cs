﻿using System;
using System.IO;
using System.Net;
using System.Net.Security;
using System.Security.Cryptography.X509Certificates;
using System.Text;

namespace Tyche.Common.Util.Utils
{
    public class HttpUtil
    {
        /// <summary>
        /// 私有构造函数
        /// </summary>
        private HttpUtil()
        {
        }

        /// <summary>
        /// 静态构造函数
        /// </summary>
        static HttpUtil()
        {
            ServicePointManager.ServerCertificateValidationCallback = new RemoteCertificateValidationCallback(CheckValidationResult);
        }

        /// <summary>
        /// CheckValidationResult
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="certificate"></param>
        /// <param name="chain"></param>
        /// <param name="errors"></param>
        /// <returns></returns>
        private static bool CheckValidationResult(object sender, X509Certificate certificate, X509Chain chain, SslPolicyErrors errors)
        {
            return true;
        }

        /// <summary>
        /// HttpGet
        /// </summary>
        /// <param name="url"></param>
        /// <param name="timeout"></param>
        /// <returns></returns>
        public static string HttpGet(string url, int timeout = 5000)
        {
            HttpWebResponse httpWebResponse = null;
            StreamReader streamReader = null;
            try
            {
                HttpWebRequest httpWebRequest = (HttpWebRequest)WebRequest.Create(url);
                httpWebRequest.Method = "GET";
                httpWebRequest.Timeout = timeout;
                httpWebResponse = (HttpWebResponse)httpWebRequest.GetResponse();
                streamReader = new StreamReader(httpWebResponse.GetResponseStream(), Encoding.UTF8);
                return streamReader.ReadToEnd();
            }
            finally
            {
                if (httpWebResponse != null)
                {
                    httpWebResponse.Close();
                }
                if (streamReader != null)
                {
                    streamReader.Close();
                }
            }
        }

        /// <summary>
        /// HttpGet
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="url"></param>
        /// <param name="converter"></param>
        /// <param name="timeout"></param>
        /// <returns></returns>
        public static T HttpGet<T>(string url, Func<string, T> converter, int timeout = 5000)
        {
            return converter(HttpGet(url, timeout));
        }

        /// <summary>
        /// HttpPost
        /// </summary>
        /// <param name="url"></param>
        /// <param name="content"></param>
        /// <param name="contentType"></param>
        /// <param name="timeout"></param>
        /// <returns></returns>
        public static string HttpPost(string url, string content, string contentType = "application/x-www-form-urlencoded", int timeout = 5000)
        {
            HttpWebResponse httpWebResponse = null;
            StreamReader streamReader = null;
            Stream stream = null;
            try
            {
                HttpWebRequest httpWebRequest = (HttpWebRequest)WebRequest.Create(url);
                httpWebRequest.Method = "POST";
                httpWebRequest.Timeout = timeout;
                httpWebRequest.ContentType = contentType;
                byte[] data = Encoding.UTF8.GetBytes(content);
                stream = httpWebRequest.GetRequestStream();
                stream.Write(data, 0, data.Length);
                stream.Close();
                httpWebResponse = (HttpWebResponse)httpWebRequest.GetResponse();
                streamReader = new StreamReader(httpWebResponse.GetResponseStream(), Encoding.UTF8);
                return streamReader.ReadToEnd();
            }
            finally
            {
                if (httpWebResponse != null)
                {
                    httpWebResponse.Close();
                }
                if (streamReader != null)
                {
                    streamReader.Close();
                }
                if (stream != null)
                {
                    stream.Close();
                }
            }
        }

        /// <summary>
        /// HttpPost
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="url"></param>
        /// <param name="content"></param>
        /// <param name="converter"></param>
        /// <param name="contentType"></param>
        /// <returns></returns>
        public static T HttpPost<T>(string url, string content, Func<string, T> converter, string contentType = "application/x-www-form-urlencoded", int timeout = 5000)
        {
            return converter(HttpPost(url, content, contentType, timeout));
        }
    }
}
