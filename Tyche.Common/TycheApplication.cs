﻿using System;
using System.Linq;
using System.Reflection;
using System.Reflection.Emit;
using Tyche.Common.Resource;

namespace Tyche.Common
{
    public class TycheApplication
    {
        public static void Bless()
        {
            var moduleBuilder = AppDomain.CurrentDomain
                .DefineDynamicAssembly(new AssemblyName("Tyche.PROXYASSEMBLY"), AssemblyBuilderAccess.Run)
                .DefineDynamicModule("PROXIES");

            foreach (var resource in TycheResourceManager.GetResources().Where(r => r.Builder != null))
            {
                resource.Builder.Build(moduleBuilder, resource.Interface);
            }
        }
    }
}
