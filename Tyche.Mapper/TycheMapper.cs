﻿using Dapper;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using Tyche.DAO.Mapper;

namespace Tyche.Mapper
{
    public class TycheMapper : ITycheMapper
    {
        /// <summary>
        /// 数据库连接
        /// </summary>
        private IDbConnection connection { get; set; }

        /// <summary>
        /// 构造函数
        /// </summary>
        public TycheMapper()
        {
            connection = new SqlConnection(ConfigurationManager.AppSettings["TycheConnectString"]);
        }

        /// <summary>
        /// 打开数据库连接
        /// </summary>
        public void Open()
        {
            if (connection.State != ConnectionState.Open)
            {
                connection.Open();
            }
        }

        /// <summary>
        /// 关闭数据库连接
        /// </summary>
        public void Close()
        {
            if (connection.State == ConnectionState.Open)
            {
                connection.Close();
                connection.Dispose();
            }
        }

        /// <summary>
        /// Dispose
        /// </summary>
        public void Dispose()
        {
            Close();
        }

        /// <summary>
        /// 开启事务
        /// </summary>
        /// <param name="level"></param>
        /// <returns></returns>
        public IDbTransaction BeginTransaction(IsolationLevel level)
        {
            Open();
            return connection.BeginTransaction(level);
        }

        /// <summary>
        /// 插入
        /// </summary>
        /// <param name="sql"></param>
        /// <param name="parameter"></param>
        /// <param name="transaction"></param>
        /// <returns></returns>
        public int Insert(string sql, object parameter, IDbTransaction transaction = null)
        {
            Open();
            return connection.Execute(sql, parameter, transaction);
        }

        /// <summary>
        /// 插入后返回自增主键
        /// </summary>
        /// <param name="sql"></param>
        /// <param name="parameter"></param>
        /// <param name="transaction"></param>
        /// <returns></returns>
        public int InsertAndReturnId(string sql, object parameter = null, IDbTransaction transaction = null)
        {
            Open();
            return Convert.ToInt32(connection.ExecuteScalar(sql + "; SELECT SCOPE_IDENTITY()", parameter, transaction));
        }

        /// <summary>
        /// 搜索多条记录
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="sql"></param>
        /// <param name="parameter"></param>
        /// <returns></returns>
        public IEnumerable<T> Select<T>(string sql, object parameter = null)
        {
            Open();
            return connection.Query<T>(sql, parameter);
        }

        /// <summary>
        /// 搜索单条记录
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="sql"></param>
        /// <param name="parameter"></param>
        /// <returns></returns>
        public T SelectSingle<T>(string sql, object parameter = null)
        {
            Open();
            return connection.QueryFirstOrDefault<T>(sql, parameter);
        }

        /// <summary>
        /// 更新
        /// </summary>
        /// <param name="sql"></param>
        /// <param name="parameter"></param>
        /// <param name="transaction"></param>
        /// <returns></returns>
        public int Update(string sql, object parameter = null, IDbTransaction transaction = null)
        {
            Open();
            return connection.Execute(sql, parameter, transaction);
        }
    }
}
