﻿using System;

namespace Tyche.Common.Filter
{
    [AttributeUsage(AttributeTargets.Method)]
    public abstract class TycheFilterAttribute : Attribute, ITycheFilter
    {
        /// <summary>
        /// 方法执行前处理
        /// </summary>
        /// <param name="method"></param>
        /// <param name="parameters"></param>
        public virtual void Executing(string method, params object[] parameters)
        {
        }

        /// <summary>
        /// 方法执行完成
        /// </summary>
        /// <param name="result"></param>
        public virtual void Executed(object result)
        {
        }

        /// <summary>
        /// 方法中断
        /// </summary>
        /// <param name="returnType"></param>
        /// <returns></returns>
        public virtual object Interrupt(Type returnType)
        {
            return null;
        }

        /// <summary>
        /// 方法异常处理
        /// </summary>
        /// <param name="exception"></param>
        public virtual void Exception(Exception exception)
        {
        }
    }
}
