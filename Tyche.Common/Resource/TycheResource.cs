﻿using System;
using Tyche.Common.ProxyBuilder;

namespace Tyche.Common.Resource
{
    public class TycheResource
    {
        /// <summary>
        /// 资源名
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// 接口类型
        /// </summary>
        public Type Interface { get; set; }

        /// <summary>
        /// 代理类型
        /// </summary>
        public Type Proxy { get; set; }

        /// <summary>
        /// 实现类型
        /// </summary>
        public Type Implement { get; set; }

        /// <summary>
        /// 类型
        /// </summary>
        public ITycheProxyBuilder Builder { get; set; }
    }
}
