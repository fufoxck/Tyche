﻿using System;
using System.Collections.Generic;
using System.Data;

namespace Tyche.DAO.Mapper
{
    public interface ITycheMapper: IDisposable
    {
        /// <summary>
        /// 搜索多条记录
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="sql"></param>
        /// <param name="parameter"></param>
        /// <returns></returns>
        IEnumerable<T> Select<T>(string sql, object parameter = null);

        /// <summary>
        /// 搜索单条记录
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="sql"></param>
        /// <param name="parameter"></param>
        /// <returns></returns>
        T SelectSingle<T>(string sql, object parameter = null);

        /// <summary>
        /// 插入
        /// </summary>
        /// <param name="sql"></param>
        /// <param name="parameter"></param>
        /// <param name="transaction"></param>
        /// <returns></returns>
        int Insert(string sql, object parameter, IDbTransaction transaction = null);

        /// <summary>
        /// 插入后返回自增主键
        /// </summary>
        /// <param name="sql"></param>
        /// <param name="parameter"></param>
        /// <param name="transaction"></param>
        /// <returns></returns>
        int InsertAndReturnId(string sql, object parameter = null, IDbTransaction transaction = null);

        /// <summary>
        /// 更新
        /// </summary>
        /// <param name="sql"></param>
        /// <param name="parameter"></param>
        /// <param name="transaction"></param>
        /// <returns></returns>
        int Update(string sql, object parameter = null, IDbTransaction transaction = null);

        /// <summary>
        /// 开启事务
        /// </summary>
        /// <param name="level"></param>
        /// <returns></returns>
        IDbTransaction BeginTransaction(IsolationLevel level);
    }
}
