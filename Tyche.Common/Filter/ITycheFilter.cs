﻿using System;

namespace Tyche.Common.Filter
{
    public interface ITycheFilter
    {
        /// <summary>
        /// 方法执行前处理
        /// </summary>
        /// <param name="method"></param>
        /// <param name="parameters"></param>
        void Executing(string method, params object[] parameters);

        /// <summary>
        /// 方法执行完成
        /// </summary>
        /// <param name="result"></param>
        void Executed(object result);

        /// <summary>
        /// 方法中断
        /// </summary>
        /// <param name="returnType"></param>
        /// <returns></returns>
        object Interrupt(Type returnType);

        /// <summary>
        /// 方法异常处理
        /// </summary>
        /// <param name="exception"></param>
        void Exception(Exception exception);
    }
}
